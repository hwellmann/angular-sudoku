import { Component, Inject } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

interface Data {
    title: string;
    question: string;
}

@Component({
    templateUrl: "./confirm.component.html",
})
export class ConfirmComponent {
    constructor(
        public dialogRef: MatDialogRef<ConfirmComponent>,
        @Inject(MAT_DIALOG_DATA) readonly data: Data,
    ) { }
}

export function confirm(callHandlerWithoutDialog: boolean, dialog: MatDialog, title: string,
    question: string, confirmHandler: () => void): void {
    if (callHandlerWithoutDialog) {
        confirmHandler();
    } else {
        const dialogRef = dialog.open<ConfirmComponent, Data>(ConfirmComponent,
            { data: { question: question, title: title } });

        dialogRef
            .afterClosed()
            .subscribe((result: boolean) => {
                if (result) {
                    confirmHandler();
                }
            });
    }
}
